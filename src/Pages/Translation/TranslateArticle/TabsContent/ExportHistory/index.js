import React from 'react';
import { Grid, Card, Progress, Pagination, Button, Dropdown, Icon, Modal, Input, Checkbox, Label } from 'semantic-ui-react';
import { connect } from 'react-redux';


import LoaderComponent from '../../../../../shared/components/LoaderComponent';
import * as translateArticleActions from '../../modules/actions';
import * as pollerActions from '../../../../../actions/poller';
import TranslationExportCard from './TranslationExportCard';
import _ from 'lodash';

const FETCH_TRANSLATIONEXPORTS = 'FETCH_TRANSLATIONEXPORTS';


class ExportHistory extends React.Component {
    state = {
        showProgress: false,
        selectedTranslationExport: null,
        translationExportModalVisible: false,
    }
    componentWillMount = () => {
        this.props.setExportHistoryPageNumber(1);
        this.props.fetchTranslationExports(this.props.exportHistoryCurrentPageNumber, true);
        this.props.fetchArticleVideo(this.props.translatableArticle.video);
        this.props.startJob({ jobName: FETCH_TRANSLATIONEXPORTS, interval: 10000 }, () => {
            this.props.fetchTranslationExports(this.props.exportHistoryCurrentPageNumber, false);
        })
        setInterval(() => {
            this.setState({ showProgress: !this.state.showProgress });
        }, 5000);
    }

    componentWillUnmount = () => {
        this.props.stopJob(FETCH_TRANSLATIONEXPORTS);
        this.props.setExportHistoryPageNumber(1);
    }

    onPageChange = (e, { activePage }) => {
        this.props.setExportHistoryPageNumber(activePage);
        this.props.fetchTranslationExports(activePage, true);
    }

    onDeclineRequest = translationExport => {
        this.props.declineTranslationExport(translationExport._id);
    }

    onApproveRequest = translationExport => {
        this.props.approveTranslationExport(translationExport._id)
    }

    canApprove = () => {
        const userRole = this.props.user.organizationRoles.find((r) => r.organization._id === this.props.organization._id)
        return userRole && (userRole.organizationOwner || userRole.permissions.indexOf('admin') !== -1);
    }

    onOpenTranslationExportSettings = (translationExport) => {
        this.setState({ selectedTranslationExport: translationExport, translationExportModalVisible: true });
    }

    onUpdateTranslationExport = () => {
        const { selectedTranslationExport } = this.state;
        this.setState({ selectedTranslationExport: null, translationExportModalVisible: false });
        const changes = {
            voiceVolume: selectedTranslationExport.voiceVolume,
            backgroundMusicVolume: selectedTranslationExport.backgroundMusicVolume,
            normalizeAudio: selectedTranslationExport.normalizeAudio,
        };
        this.props.updateTranslationExportAudioSettings(selectedTranslationExport._id, changes);
    }

    renderTranslationExportSettings = () => {
        const { selectedTranslationExport, translationExportModalVisible } = this.state;
        return (
            <Modal
                size='tiny'
                open={translationExportModalVisible}
                onClose={() => this.setState({ translationExportModalVisible: false })}
            >
                <Modal.Header>
                    <h3>Audio Settings</h3>
                </Modal.Header>
                {selectedTranslationExport && (

                    <Modal.Content
                        style={{}}
                    >
                        <Grid>
                            <Grid.Row>
                                <Grid.Column width={2} />
                                <Grid.Column width={6}>
                                    Audio Mastering
                                </Grid.Column>
                                <Grid.Column width={6} style={{ display: 'flex', justifyContent: 'center' }}>
                                    <Checkbox
                                        style={{ marginLeft: 10 }}
                                        onChange={(e, { checked }) => {
                                            this.setState({ selectedTranslationExport: { ...selectedTranslationExport, normalizeAudio: checked } });
                                        }}
                                        checked={selectedTranslationExport.normalizeAudio}
                                    />
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row>
                                <Grid.Column width={2} />
                                <Grid.Column width={6}>
                                    Voice Volume:
                                </Grid.Column>
                                <Grid.Column width={6} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>

                                    <Input
                                        style={{ marginLeft: 10 }}
                                        type="number"
                                        label={{ basic: true, content: '%' }}
                                        labelPosition="right"
                                        min={0}
                                        max={1000}
                                        step={10}
                                        value={parseInt(selectedTranslationExport.voiceVolume * 100)}
                                        onChange={(e) => {
                                            this.setState({ selectedTranslationExport: { ...selectedTranslationExport, voiceVolume: e.target.value / 100 } });
                                        }}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column width={2} />
                                <Grid.Column width={6}>
                                    Background Music Volume:
                                </Grid.Column>
                                <Grid.Column width={6} style={{ display: 'flex', justifyContent: 'center' }}>
                                    {this.props.video && this.props.video.backgroundMusicUrl ? (
                                        <Input
                                            style={{ marginLeft: 10 }}
                                            label={{ basic: true, content: '%' }}
                                            labelPosition="right"
                                            type="number"
                                            min={0}
                                            max={1000}
                                            step={10}
                                            value={parseInt(selectedTranslationExport.backgroundMusicVolume * 100)}
                                            onChange={(e) => {
                                                this.setState({ selectedTranslationExport: { ...selectedTranslationExport, backgroundMusicVolume: e.target.value / 100 } });
                                            }}
                                        />
                                    ) : (
                                            <strong>No Background Music uploaded yet.</strong>
                                        )}
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Modal.Content>
                )}
                <Modal.Actions>
                    <Button onClick={() => this.setState({ translationExportModalVisible: false })}>Cancel</Button>
                    <Button primary
                        onClick={this.onUpdateTranslationExport}
                    >
                        Update
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }

    renderPagination = () => (
        <Grid.Row>
            <Grid.Column width={16}>
                <div className="pull-right">
                    <Pagination
                        activePage={this.props.exportHistoryCurrentPageNumber}
                        onPageChange={this.onPageChange}
                        totalPages={this.props.exportHistoryTotalPages}
                    />
                </div>
            </Grid.Column>
        </Grid.Row>
    )


    render() {
        return (
            <LoaderComponent active={this.props.loading}>
                <div>
                    <Grid>
                        {this.renderPagination()}
                        {(!this.props.translationExports || this.props.translationExports.length === 0) && (
                            <Grid.Row>
                                <Grid.Column>
                                    No exports are available here
                                    </Grid.Column>
                            </Grid.Row>
                        )}
                        {this.props.translationExports && _.chunk(this.props.translationExports, 3).map((translationExportChunk, index) => (
                            <Grid.Row key={`translation-export-chunk-${index}`} >
                                {translationExportChunk.map((translationExport) => (
                                    <Grid.Column width={5} key={translationExport._id}>
                                        <TranslationExportCard
                                            title={this.props.translatableArticle.title}
                                            lang={this.props.translatableArticle.langCode || this.props.translatableArticle.langName}
                                            translationExport={translationExport}
                                            canApprove={this.canApprove()}
                                            hasBackgroundMusic={this.props.video && this.props.video.backgroundMusicUrl}
                                            onOpenTranslationExportSettings={this.onOpenTranslationExportSettings}
                                            onGenerateTranslationExportSubtitledVideo={this.props.generateTranslationExportSubtitledVideo}
                                            onGenerateTranslationExportSubtitle={this.props.generateTranslationExportSubtitle}
                                            onGenerateTranslationExportAudioArchive={this.props.generateTranslationExportAudioArchive}
                                            onApproveRequest={this.onApproveRequest}
                                            onDeclineRequest={this.onDeclineRequest}
                                        />
                                    </Grid.Column>
                                ))}
                            </Grid.Row>
                        ))}
                        {this.renderTranslationExportSettings()}
                    </Grid>
                </div>
            </LoaderComponent>
        )
    }
}

const mapStateToProps = ({ translateArticle, authentication, organization }) => ({
    translationExports: translateArticle.translationExports,
    exportHistoryCurrentPageNumber: translateArticle.exportHistoryCurrentPageNumber,
    exportHistoryTotalPages: translateArticle.exportHistoryTotalPages,
    loading: translateArticle.loading,
    organization: organization.organization,
    user: authentication.user,
    translatableArticle: translateArticle.translatableArticle,
    video: translateArticle.video,
})

const mapDispatchToProps = (dispatch) => ({
    fetchTranslationExports: (loading) => dispatch(translateArticleActions.fetchTranslationExports(loading)),
    setExportHistoryPageNumber: pageNumber => dispatch(translateArticleActions.setExportHistoryPageNumber(pageNumber)),
    generateTranslationExportAudioArchive: translationExportId => dispatch(translateArticleActions.generateTranslationExportAudioArchive(translationExportId)),
    generateTranslationExportSubtitledVideo: translationExportId => dispatch(translateArticleActions.generateTranslationExportSubtitledVideo(translationExportId)),
    generateTranslationExportSubtitle: translationExportId => dispatch(translateArticleActions.generateTranslationExportSubtitle(translationExportId)),
    updateTranslationExportAudioSettings: (translationExportId, changes) => dispatch(translateArticleActions.updateTranslationExportAudioSettings(translationExportId, changes)),
    approveTranslationExport: (translationExportId) => dispatch(translateArticleActions.approveTranslationExport(translationExportId)),
    declineTranslationExport: (translationExportId) => dispatch(translateArticleActions.declineTranslationExport(translationExportId)),
    startJob: (options, callFunc) => dispatch(pollerActions.startJob(options, callFunc)),
    stopJob: (jobName) => dispatch(pollerActions.stopJob(jobName)),
    fetchArticleVideo: videoId => dispatch(translateArticleActions.fetchArticleVideo(videoId)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ExportHistory); 