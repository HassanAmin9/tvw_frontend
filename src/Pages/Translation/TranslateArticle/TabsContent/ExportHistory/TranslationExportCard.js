import React from 'react';
import { Card, Dropdown, Button, Progress, Icon, Label } from 'semantic-ui-react';
import fileUtils from '../../../../../shared/utils/fileUtils';
import moment from 'moment';
import ProgressButton from '../../../../../shared/components/ProgressButton';
import _ from 'lodash';

function renderLabel(text) {
    return (
        <Label style={{ margin: '5px 5px' }} color="blue" >
            {text}
        </Label>
    )
}
export default class TranslationExportCard extends React.Component {

    renderDownloadDropdown = () => {
        const { translationExport } = this.props;
        if (translationExport.status !== 'done') return;

        return (
            <a href="javascript:void(0)" className="pull-right">
                <Dropdown pointing text='Download'>
                    <Dropdown.Menu style={{ color: 'black !important', minWidth: 200 }}>
                        <Dropdown.Item
                            as={'a'}
                            style={{ color: 'black' }}
                            href={`${translationExport.videoUrl}?download`}
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Video
                            <Icon style={{ marginLeft: 10 }} name="check circle outline" color="green" />
                        </Dropdown.Item>

                        {translationExport.subtitledVideoUrl ? (
                            <Dropdown.Item
                                as={'a'}
                                style={{ color: 'black' }}
                                href={`${translationExport.subtitledVideoUrl}?download`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Video + Subtitles
                                <Icon style={{ marginLeft: 10 }} name="check circle outline" color="green" />

                            </Dropdown.Item>
                        ) : (

                                <Dropdown.Item
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        if (!translationExport.subtitledVideoUrl) {
                                            this.props.onGenerateTranslationExportSubtitledVideo(translationExport._id);
                                        } else {
                                            fileUtils.downloadFile(translationExport.subtitledVideoUrl);
                                        }
                                    }}

                                >
                                    <ProgressButton
                                        onClick={() => {
                                        }}
                                        percent={translationExport.subtitledVideoProgress}
                                        showProgress={translationExport.subtitledVideoProgress > 0}
                                        text="Video + Subtitles"
                                    />
                                </Dropdown.Item>
                            )}

                        {translationExport.subtitleUrl ? (
                            <Dropdown.Item
                                as={'a'}
                                style={{ color: 'black' }}
                                href={`${translationExport.subtitleUrl}?download`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Subtitles
                                <Icon style={{ marginLeft: 10 }} name="check circle outline" color="green" />

                            </Dropdown.Item>
                        ) : (

                                <Dropdown.Item
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        if (!translationExport.subtitleUrl) {
                                            this.props.onGenerateTranslationExportSubtitle(translationExport._id);
                                            // this.props.generateTranslationExportSubtitledVideo(translationExport._id);
                                        } else {
                                            fileUtils.downloadFile(translationExport.subtitleUrl);
                                        }
                                    }}

                                >
                                    <ProgressButton
                                        onClick={() => {
                                        }}
                                        percent={translationExport.subtitleProgress}
                                        showProgress={translationExport.subtitleProgress > 0}
                                        text="Subtitles"
                                    />
                                </Dropdown.Item>
                            )}
                        {translationExport.audiosArchiveUrl ? (

                            <Dropdown.Item
                                as="a"
                                style={{ color: 'black' }}
                                href={`${translationExport.audiosArchiveUrl}?download`}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                Audios
                                <Icon style={{ marginLeft: 10 }} name="check circle outline" color="green" />

                            </Dropdown.Item>
                        ) : (
                                <Dropdown.Item
                                    onClick={(e) => {
                                        e.stopPropagation();
                                        if (!translationExport.audiosArchiveUrl) {
                                            this.props.onGenerateTranslationExportAudioArchive(translationExport._id);
                                        } else {
                                            fileUtils.downloadFile(translationExport.audiosArchiveUrl);
                                        }
                                    }}
                                >
                                    <ProgressButton
                                        percent={translationExport.audiosArchiveProgress}
                                        showProgress={translationExport.audiosArchiveProgress > 0}
                                        text="Audios"
                                    />
                                </Dropdown.Item>
                            )}


                    </Dropdown.Menu>
                </Dropdown>
            </a>
        )
    }

    render() {
        const { translationExport, canApprove } = this.props;
        return (
            <Card fluid>
                <Card.Content>
                    <video width={'100%'} controls src={translationExport.videoUrl} />
                </Card.Content>

                <Card.Content>
                    {translationExport.exportRequestStatus === 'approved' && translationExport.version && (
                        <p>
                            Version: {translationExport.version}.{translationExport.subVersion}
                        </p>
                    )}
                    <p style={{ textTransform: 'capitalize' }}>
                        <strong>Status:</strong>{'\t'}
                        {translationExport.exportRequestStatus === 'declined' && <span>Declined</span>}
                        {translationExport.exportRequestStatus === 'pending' && (<span>Pending approval</span>)}
                        {translationExport.exportRequestStatus === 'approved' && translationExport.status}

                        {translationExport.exportRequestStatus === 'pending' && (
                            <Button
                                icon="setting"
                                circular
                                primary
                                className="pull-right"
                                onClick={() => this.props.onOpenTranslationExportSettings(translationExport)}
                            />
                        )}
                        {this.renderDownloadDropdown()}
                    </p>

                    {translationExport.exportRequestStatus === 'approved' && (
                        <Progress progress indicating percent={translationExport.progress} />
                    )}

                    {translationExport.exportRequestBy && (
                        <p><strong>Requested by:</strong> {translationExport.exportRequestBy.email}</p>
                    )}
                    {translationExport.approvedBy && (
                        <p><strong>Approved by:</strong> {translationExport.approvedBy.email}</p>
                    )}

                    {translationExport.declinedBy && (
                        <p><strong>Declined by:</strong> {translationExport.declinedBy.email}</p>
                    )}
                    {translationExport.translationBy && translationExport.translationBy.length > 0 && (
                        <div>
                            <strong>Translations by:</strong>
                            <ul>

                                {translationExport.translationBy && translationExport.translationBy.map((u) => (
                                    <li key={`translation_by-${u.email}`}>{u.email}</li>
                                ))}
                            </ul>
                        </div>
                    )}
                    <p style={{ textAlign: 'right' }}>{moment(translationExport.created_at).format('hh:mm a DD/MM/YYYY')}</p>
                </Card.Content>
                {translationExport.status === 'done' && (
                    <Card.Content style={{ display: 'flex', justifyContent: 'flex-start', flexWrap: 'wrap' }}>
                        {renderLabel('ML Background Noise Cancellation')}
                        {translationExport.normalizeAudio && renderLabel('Audio Mastering')}
                        {renderLabel(`Vocal Level: ${translationExport.voiceVolume}x`)}
                        {translationExport.backgroundMusicTransposed && renderLabel(`ML Background Music Transpose`)}
                        {translationExport.hasBackgroundMusic && renderLabel(`Background Music Level: ${translationExport.backgroundMusicVolume}x`)}
                    </Card.Content>
                )}

                {canApprove && translationExport.exportRequestStatus === 'pending' && (
                    <Card.Content>
                        <div className='pull-right'>
                            <Button color="red" onClick={() => this.props.onDeclineRequest(translationExport)}>
                                Decline
                            </Button>
                            <Button color="blue" onClick={() => this.props.onApproveRequest(translationExport)}>
                                Approve
                            </Button>
                        </div>
                    </Card.Content>
                )}

            </Card>
        )
    }
}