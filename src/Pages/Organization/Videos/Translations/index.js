import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Grid, Card, Progress, Button, Pagination, Modal, Input } from 'semantic-ui-react';

import LoaderComponent from '../../../../shared/components/LoaderComponent';
import ReactPlayer from 'react-player';
import { isoLangs, supportedLangs } from '../../../../shared/constants/langs';
import routes from '../../../../shared/routes';

import * as videoActions from '../modules/actions';
import * as organizationActions from '../../../../actions/organization';
import AddHumanVoiceModal from '../../../../shared/components/AddHumanVoiceModal';
import VideosTabs from '../VideosTabs';
import RoleRenderer from '../../../../shared/containers/RoleRenderer';
import ArticleSummaryCard from '../../../../shared/components/ArticleSummaryCard';
import { debounce, showMoreText, getTranslationArticleUrl, getUsersByRoles } from '../../../../shared/utils/helpers';
import ShowMore from '../../../../shared/components/ShowMore';

class Translated extends React.Component {
    constructor(props) {
        super(props);
        this.debouncedSearch = debounce((searchTerm) => {
            this.props.setCurrentPageNumber(1);
            this.props.fetchTranslatedArticles();
        }, 500)

    }
    componentWillMount = () => {
        this.props.setSearchFilter('');
        this.props.setCurrentPageNumber(1);
        this.props.fetchTranslatedArticles();
        this.props.fetchUsers(this.props.organization._id);
    }

    getLanguage = langCode => {
        const fromOthers = isoLangs[langCode];
        if (fromOthers) return fromOthers.name;
        const fromSupported = supportedLangs.find(l => l.code === langCode);
        if (fromSupported) return fromSupported.name;
        return langCode
    }

    onPageChange = (e, { activePage }) => {
        this.props.setCurrentPageNumber(activePage);
        this.props.fetchTranslatedArticles(this.props.organization._id, activePage);
    }

    onSearchChange = (searchTerm) => {
        this.props.setSearchFilter(searchTerm);
        this.debouncedSearch()
    }



    onAddHumanVoice = (langCode, langName, translators) => {
        const { video } = this.props.selectedVideo;
        this.props.setAddHumanVoiceModalVisible(false);
        this.props.generateTranslatableArticle(video.article, langCode, langName, translators);
    }


    getTranslators = () => {
        return getUsersByRoles(this.props.organizationUsers, this.props.organization, ['translate', 'admin', 'owner']);
    }

    renderPagination = () => (
        <Pagination
            style={{ marginLeft: 20 }}
            activePage={this.props.currentPageNumber}
            onPageChange={this.onPageChange}
            totalPages={this.props.totalPagesCount}
        />
    )


    _renderAddHumanVoiceModal() {
        const users = this.getTranslators();
        const { selectedVideo } = this.props;
        if (!selectedVideo) return null;
        const disabledLanguages = this.props.selectedVideo && this.props.selectedVideo.articles ? this.props.selectedVideo.articles.map(a => a.langCode) : [];

        return (
            <AddHumanVoiceModal
                open={this.props.addHumanVoiceModalVisible}
                onClose={() => this.props.setAddHumanVoiceModalVisible(false)}
                users={users}
                speakersProfile={selectedVideo && selectedVideo.originalArticle ? selectedVideo.originalArticle.speakersProfile : []}
                disabledLanguages={disabledLanguages}
                skippable={false}
                onSubmit={(langCode, langName, translators) => this.onAddHumanVoice(langCode, langName, translators)}
            />
        )
    }

    render() {
        return (
            <div>
                <VideosTabs />
                <Grid style={{ textAlign: 'center', marginTop: '1rem' }}>
                    <RoleRenderer roles={['admin', 'translate']}>
                        <Grid.Row style={{ marginBottom: 20 }}>
                            <Grid.Column width={5}>

                                <Input
                                    fluid
                                    style={{ height: '100%' }}
                                    type="text"
                                    icon="search"
                                    iconPosition="left"
                                    input={<input
                                        type="text"
                                        style={{ borderRadius: 20, color: '#999999', backgroundColor: '#d4e0ed' }}
                                    />}
                                    placeholder="Search by file name, video name, etc"
                                    value={this.props.searchFilter}
                                    onChange={(e, { value }) => this.onSearchChange(value)}
                                />
                            </Grid.Column>
                            <Grid.Column width={11}>
                                <div className="pull-right">
                                    {this.renderPagination()}
                                </div>
                            </Grid.Column>
                        </Grid.Row>
                        <LoaderComponent active={this.props.videosLoading}>
                            <Grid.Row>
                                {this.props.translatedArticles.map((translatedArticle) => (
                                    <Grid.Column width={4} key={`translated-article-container-${translatedArticle.video._id}`} style={{ marginBottom: 30 }}>

                                        <Card fluid >
                                            <Card.Header style={{ margin: 10, wordBreak: 'break-word' }}>
                                                <Link to={routes.organziationTranslationMetrics(translatedArticle.video._id)}>
                                                    <h3>
                                                        <ShowMore length={25} text={translatedArticle.video.title} />
                                                    </h3>
                                                </Link>
                                            </Card.Header>
                                            <Card.Content>
                                                <ReactPlayer
                                                    url={translatedArticle.video.url}
                                                    width="100%"
                                                    height="150px"
                                                    controls
                                                    light={translatedArticle.video.thumbnailUrl || false}
                                                    playing={translatedArticle.video.thumbnailUrl ? true : false}
                                                    preload={"false"}
                                                />
                                                {/* <video src={translatedArticle.video.url} width="100%" height="100%" controls preload="false" /> */}
                                            </Card.Content>
                                            <Card.Content style={{ textAlign: 'left' }}>
                                                <p>
                                                    <strong>Translated in: &nbsp;</strong>
                                                </p>
                                                <p
                                                    style={{ wordBreak: 'break-word' }}
                                                >
                                                    {/* routes.translationArticle(singleTranslatedArticle.video.article) + `?lang=${article.langCode}` */}
                                                    {translatedArticle.articles.map((article, index) => (
                                                        <Link key={`translated-article-adadad-${article._id}`} to={routes.translationArticle(article._id)}>
                                                            {article.langName ? article.langName : isoLangs[article.langCode] ? isoLangs[article.langCode].name : article.langCode}
                                                            {article.tts && '< TTS >'}
                                                            {index !== translatedArticle.articles.length - 1 && (
                                                                <span>,&nbsp;</span>
                                                            )}
                                                        </Link>
                                                    ))}
                                                </p>
                                                <div style={{ textAlign: 'right', marginTop: 20 }}>

                                                    <Link to={routes.organizationArticle(translatedArticle.video.article)}>
                                                        slides
                                                </Link>
                                                </div>
                                            </Card.Content>
                                            <Button fluid color="blue" onClick={() => {
                                                this.props.setSelectedVideo(translatedArticle);
                                                this.props.setAddHumanVoiceModalVisible(true);
                                            }}>Translate</Button>
                                        </Card>

                                    </Grid.Column>
                                ))}
                            </Grid.Row>

                            {this._renderAddHumanVoiceModal()}
                        </LoaderComponent>
                    </RoleRenderer>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = ({ organization, authentication, organizationVideos }) => ({
    organization: organization.organization,
    user: authentication.user,
    translatedArticles: organizationVideos.translatedArticles,
    videos: organizationVideos.videos,
    languageFilter: organizationVideos.languageFilter,
    videosLoading: organizationVideos.videosLoading,
    totalPagesCount: organizationVideos.totalPagesCount,
    selectedVideo: organizationVideos.selectedVideo,
    addHumanVoiceModalVisible: organizationVideos.addHumanVoiceModalVisible,
    currentPageNumber: organizationVideos.currentPageNumber,
    searchFilter: organizationVideos.searchFilter,
    organizationUsers: organization.users,

})
const mapDispatchToProps = (dispatch) => ({
    setSelectedVideo: video => dispatch(videoActions.setSelectedVideo(video)),
    setAddHumanVoiceModalVisible: visible => dispatch(videoActions.setAddHumanVoiceModalVisible(visible)),
    setCurrentPageNumber: pageNumber => dispatch(videoActions.setCurrentPageNumber(pageNumber)),
    fetchTranslatedArticles: () => dispatch(videoActions.fetchTranslatedArticles()),
    deleteArticle: (articleId) => dispatch(videoActions.deleteArticle(articleId)),
    generateTranslatableArticle: (originalArticleId, langCode, langName, translators) => dispatch(videoActions.generateTranslatableArticle(originalArticleId, langCode, langName, translators)),
    fetchUsers: (organizationId) => dispatch(organizationActions.fetchUsers(organizationId)),
    setSearchFilter: filter => dispatch(videoActions.setSearchFilter(filter)),
})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Translated));
