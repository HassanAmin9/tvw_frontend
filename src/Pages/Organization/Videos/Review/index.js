import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Grid, Dropdown, Pagination, Input, Modal, Button, Card, Popup, Icon } from 'semantic-ui-react';

import * as videoActions from '../modules/actions';
import VideosTabs from '../VideosTabs';

import Tabs from '../../../../shared/components/Tabs';
import Proofread from './TabsContent/Proofread';
import Completed from './TabsContent/Completed';
import Transcribe from './TabsContent/Transcribe';
import queryString from 'query-string';
import * as organizationActions from '../../../../actions/organization';

import LoaderComponent from '../../../../shared/components/LoaderComponent';
import websockets from '../../../../websockets';
import NotificationService from '../../../../shared/utils/NotificationService';
import VideoCard from '../../../../shared/components/VideoCard';

import { supportedLangs, isoLangsArray } from '../../../../shared/constants/langs';
import { debounce, getUserNamePreview, getUsersByRoles } from '../../../../shared/utils/helpers';
import RoleRenderer from '../../../../shared/containers/RoleRenderer';
import ReactAvatar from 'react-avatar';
import AssignReviewUsers from '../../../../shared/components/AssignReviewUsers';
import routes from '../../../../shared/routes';
import EditVideoModal from '../../../../shared/components/EditVideoModal';
let langsToUse = supportedLangs.map((l) => ({ ...l, supported: true }));
langsToUse = langsToUse.concat(isoLangsArray.filter((l) => supportedLangs.every((l2) => l2.code.indexOf(l.code) === -1)));
const langsOptions = langsToUse.map((lang) => ({ key: lang.code, value: lang.code, text: `${lang.name} ( ${lang.code} )` }));


const tabsIndexMap = {
    0: 'transcribe',
    1: 'proofread',
    2: 'completed'
}

const videoStatusMap = {
    0: ['uploaded', 'uploading', 'transcriping', 'cutting'],
    1: ['proofreading', 'converting'],
    2: ['done'],
}

function formatCount(number) {
    if (number >= 10) return number;
    if (number === 0) return 0;
    return `0${number}`;
}

class Review extends React.Component {
    state = {
        activeTab: 0,
        deletedVideo: null,
        deleteVideoModalVisible: false,
        selectedVideo: null,
        assignUsersModalOpen: false,
        confirmReviewModalVisible: false,
        editVideoModalOpen: false,
        tmpEditVideo: null,
    }

    constructor(props) {
        super(props);

        this.debouncedSearch = debounce((searchTerm) => {
            this.props.setCurrentPageNumber(1);
            this.props.fetchVideos();
        }, 500)
    }

    componentDidMount = () => {
        const { activeTab } = queryString.parse(this.props.location.search);
        let tabIndex = 0;
        if (activeTab) {
            switch (activeTab) {
                case 'transcribe':
                    tabIndex = 0;
                    this.setState({ activeTab: 0 }); break;
                case 'proofread':
                    tabIndex = 1;
                    this.setState({ activeTab: 1 }); break;
                case 'completed':
                    tabIndex = 2;
                    this.setState({ activeTab: 2 }); break;
                default:
                    tabIndex = 0;
                    this.setState({ activeTab: 0 }); break;
            }
        }
        this.props.setSearchFilter('');
        this.props.fetchUsers(this.props.organization._id);
        this.props.setCurrentPageNumber(1);
        this.props.setVideoStatusFilter(videoStatusMap[tabIndex]);
        this.props.fetchVideos();
        this.props.fetchVideosCount(this.props.organization._id);
        this.videoUploadedSub = websockets.subscribeToEvent(websockets.websocketsEvents.VIDEO_UPLOADED, (data) => {
            this.props.fetchVideos();
        })

        this.videoTranscribedSub = websockets.subscribeToEvent(websockets.websocketsEvents.VIDEO_TRANSCRIBED, (video) => {
            if (this.props.videos.map((video) => video._id).indexOf(video._id) !== -1) {
                this.props.fetchVideos();
                NotificationService.info(`${video.title} Has finished transcribing and ready for Proofreading`);
            }
        })
    }



    componentWillUnmount = () => {
        if (this.videoUploadedSub) {
            websockets.unsubscribeFromEvent(websockets.websocketsEvents.VIDEO_UPLOADED)
        }
        if (this.videoTranscribedSub) {
            websockets.unsubscribeFromEvent(websockets.websocketsEvents.VIDEO_TRANSCRIBED);
        }
    }


    onTabChange = index => {
        this.setState({ activeTab: index });
        this.props.setSearchFilter('');
        this.props.setVideoStatusFilter(videoStatusMap[index]);
        this.props.setCurrentPageNumber(1);
        this.props.fetchVideos();
    }

    onPageChange = (e, { activePage }) => {
        this.props.setCurrentPageNumber(activePage);
        this.props.fetchVideos({ organization: this.props.organization._id });
    }

    onSearchChange = (searchTerm) => {
        this.props.setSearchFilter(searchTerm);
        this.debouncedSearch()
    }

    onLanguageFilterChange = (e, { value }) => {
        this.props.setLanguageFilter(value);
        this.props.setCurrentPageNumber(1)
        this.props.fetchVideos({ organization: this.props.organization._id })
    }

    onDeleteVideoClick = (video) => {
        this.setState({ deleteVideoModalVisible: true, deletedVideo: video });
    }

    deleteSelectedVideo = () => {
        this.props.deleteVideo(this.state.deletedVideo._id);
        this.setState({ deletedVideo: null, deleteVideoModalVisible: false });
    }

    onTranscribeVideo = video => {
        console.log('on review', video);
        this.props.transcribeVideo(video);
    }

    onTranscribeVideoClick = video => {
        this.props.setSelectedVideo(video);
        this.setState({ confirmReviewModalVisible: true });
    }

    navigateToConvertProgresss = videoId => {
        this.props.history.push(routes.convertProgress(videoId));
    }

    onTranscribeVideo = video => {
        console.log('on review', video);
        this.props.transcribeVideo(video);
    }
    onSkipTranscribe = video => {
        console.log('skip ', video);
        this.props.skipTranscribe(video);
    }

    onSaveAssignedUsers = (users) => {
        this.props.updateVideoReviewers(this.state.selectedVideo._id, users);
        this.setState({ selectedVideo: null, assignUsersModalOpen: false });
    }

    onSaveEditedVideo = (originalVideo, editedVideo) => {
        const changes = {};
        Object.keys(editedVideo).forEach(key => {
            if (originalVideo[key] !== editedVideo[key]) {
                changes[key] = editedVideo[key];
            }
        })
        if (changes.backgroundMusicUrl !== undefined) {
            changes.backgroundMusic = changes.backgroundMusicUrl;
            delete changes.backgroundMusicUrl;
        }
        console.log('changes', changes)
        this.props.updateVideo(originalVideo._id, changes);
        this.setState({ editVideoModalOpen: false });
    }

    onAddClick = (video) => {
        this.setState({ assignUsersModalOpen: true, selectedVideo: video });
    }

    onEditClick = video => {
        this.setState({ editVideoModalOpen: true, selectedVideo: video, tmpEditVideo: { ...video } });
    }

    renderEditVideoModal = () => (
        <EditVideoModal
            open={this.state.editVideoModalOpen}
            initialValue={this.state.selectedVideo}
            value={this.state.tmpEditVideo}
            onClose={() => this.setState({ editVideoModalOpen: false })}
            onReset={() => this.setState({ tmpEditVideo: { ...this.state.selectedVideo } })}
            onChange={(changes) => {
                const { tmpEditVideo } = this.state;
                Object.keys(changes).forEach((key) => {
                    tmpEditVideo[key] = changes[key];
                })
                this.setState({ tmpEditVideo: { ...tmpEditVideo } });
            }}
            onSave={() => this.onSaveEditedVideo(this.state.selectedVideo, this.state.tmpEditVideo)}
        />
    )

    renderPagination = () => (
        <Pagination
            activePage={this.props.currentPageNumber}
            onPageChange={this.onPageChange}
            totalPages={this.props.totalPagesCount}
        />
    )

    _renderDeleteVideoModal = () => (
        <Modal open={this.state.deleteVideoModalVisible} size="tiny">
            <Modal.Header>Delete Video</Modal.Header>
            <Modal.Content>
                Are you sure you want to delete this video? <small>(All associated content/articles will be deleted)</small>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    onClick={() => this.setState({ deleteVideoModalVisible: false, deletedVideo: null })}
                >
                    Cancel
                </Button>
                <Button
                    color="red"
                    onClick={this.deleteSelectedVideo}
                >
                    Yes
                </Button>
            </Modal.Actions>
        </Modal>
    )

    renderAssignUsers = () => (
        <AssignReviewUsers
            open={this.state.assignUsersModalOpen}
            value={this.state.selectedVideo ? this.state.selectedVideo.reviewers.map(r => r._id) : []}
            users={getUsersByRoles(this.props.organizationUsers, this.props.organization, ['admin', 'owner', 'review'])}
            onClose={() => this.setState({ assignUsersModalOpen: false, selectedVideo: null })}
            onSave={this.onSaveAssignedUsers}
        />
    )

    renderConfirmReviewModal = () => (
        <Modal open={this.state.confirmReviewModalVisible} size="tiny">
            <Modal.Header>Re-Review Video</Modal.Header>
            <Modal.Content>
                <p>Are you sure you want to re-review this video? <small><strong>( All current translations will be archived )</strong></small></p>

            </Modal.Content>
            <Modal.Actions>
                <Button onClick={() => {
                    this.setState({ confirmReviewModalVisible: false });
                    this.props.setSelectedVideo(null);
                }}>
                    Cancel
                </Button>
                <Button color="blue" onClick={() => {
                    this.setState({ confirmReviewModalVisible: false });
                    this.onTranscribeVideo(this.props.selectedVideo);
                    this.props.setSelectedVideo(null);
                }}>
                    Yes
                </Button>
            </Modal.Actions>
        </Modal>
    )

    renderVideosCards = () => {

        let renderedComp;
        if (this.state.activeTab === 0) {
            renderedComp = (
                this.props.videos && this.props.videos.length === 0 ? (
                    <div style={{ margin: 50 }}>No videos requires preview</div>
                ) : this.props.videos && this.props.videos.map((video) => {
                    const loading = ['uploading', 'transcriping', 'cutting'].indexOf(video.status) !== -1;
                    return (
                        <Grid.Column key={video._id} width={4} style={{ marginBottom: 30 }}>
                            <VideoCard
                                showOptions
                                url={video.url}
                                thumbnailUrl={video.thumbnailUrl}
                                title={video.title}
                                buttonTitle="AI Transcribe"
                                loading={loading}
                                disabled={loading}
                                onButtonClick={() => this.onTranscribeVideo(video)}
                                onDeleteVideoClick={() => this.onDeleteVideoClick(video)}
                                onEditClick={() => this.onEditClick(video)}
                                onAddClick={() => this.onAddClick(video)}
                                extra={
                                    !loading ? (
                                        <Card.Content>
                                            {video.reviewers && video.reviewers.length > 0 ? (
                                                <div style={{ margin: 20 }}>
                                                    Review Assiged to: {video.reviewers.map((reviewer) => (
                                                        <Popup
                                                            trigger={
                                                                <span>
                                                                    <ReactAvatar
                                                                        name={getUserNamePreview(reviewer)}
                                                                        style={{ margin: '0 10px', display: 'inline-block' }}
                                                                        size={30}
                                                                        round
                                                                    />
                                                                </span>
                                                            }
                                                            content={getUserNamePreview(reviewer)}
                                                        />
                                                    ))}</div>
                                            ) : null}
                                            <div className="pull-right">
                                                <Popup
                                                    content="Skip AI transcribe and proofread directly"
                                                    trigger={
                                                        <a href="javascript:void(0)" onClick={() => this.onSkipTranscribe(video)} >Skip</a>
                                                    }
                                                />
                                            </div>
                                        </Card.Content>
                                    ) : null}
                            />
                        </Grid.Column>
                    )
                })
            )
        } else if (this.state.activeTab === 1) {
            renderedComp = (
                this.props.videos && this.props.videos.length === 0 ? (
                    <div style={{ margin: 50 }}>No videos requires proofreading</div>
                ) : this.props.videos && this.props.videos.map((video) => {
                    return (
                        <Grid.Column key={video._id} width={4}>
                            <VideoCard
                                showOptions
                                url={video.url}
                                thumbnailUrl={video.thumbnailUrl}
                                title={video.title}
                                buttonTitle="Proofread"
                                loading={video.status === 'converting'}
                                disabled={video.status === 'converting'}
                                onButtonClick={() => this.navigateToConvertProgresss(video._id)}
                                onDeleteVideoClick={() => this.onDeleteVideoClick(video)}
                                onAddClick={() => this.onAddClick(video)}
                                onEditClick={() => this.onEditClick(video)}
                                extra={video.reviewers && video.reviewers.length > 0 ? (
                                    <div style={{ margin: 20 }}>
                                        Review Assiged to: {video.reviewers.map((reviewer) => (
                                            <Popup
                                                trigger={
                                                    <span>
                                                        <ReactAvatar
                                                            name={getUserNamePreview(reviewer)}
                                                            style={{ margin: '0 10px', display: 'inline-block' }}
                                                            size={30}
                                                            round
                                                        />
                                                    </span>
                                                }
                                                content={getUserNamePreview(reviewer)}
                                            />
                                        ))}</div>
                                ) : null}
                            />
                        </Grid.Column>
                    )
                })
            )
        } else {
            renderedComp = (
                this.props.videos && this.props.videos.length === 0 ? (
                    <div style={{ margin: 50 }}>No videos completed yet</div>
                ) : this.props.videos && this.props.videos.map((video) => {
                    return (
                        <Grid.Column key={video._id} width={4}>
                            <VideoCard
                                showOptions
                                url={video.url}
                                thumbnailUrl={video.thumbnailUrl}
                                title={video.title}
                                buttonTitle="Re-review"
                                onButtonClick={() => this.onTranscribeVideoClick(video)}
                                onDeleteVideoClick={() => this.onDeleteVideoClick(video)}
                                onAddClick={() => this.onAddClick(video)}
                                onEditClick={() => this.onEditClick(video)}
                                extra={video.reviewers && video.reviewers.length > 0 ? (
                                    <div style={{ margin: 20 }}>
                                        Review Assiged to: {video.reviewers.map((reviewer) => (
                                            <Popup
                                                trigger={
                                                    <span>
                                                        <ReactAvatar
                                                            name={getUserNamePreview(reviewer)}
                                                            style={{ margin: '0 10px', display: 'inline-block' }}
                                                            size={30}
                                                            round
                                                        />
                                                    </span>
                                                }
                                                content={getUserNamePreview(reviewer)}
                                            />
                                        ))}</div>
                                ) : null}
                            />
                        </Grid.Column>
                    )
                })
            )
        }
        return renderedComp
    }

    _renderTabContent = () => {

        return (
            <LoaderComponent active={this.props.videosLoading}>
                {this.renderAssignUsers()}
                {this.state.activeTab === 0 && (

                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Button
                                className="pull-right"
                                color="blue"
                                disabled={this.props.videos.length === 0}
                                onClick={() => this.onTranscribeVideo({ _id: 'all' })}
                            >Transcribe All</Button>
                        </Grid.Column>
                    </Grid.Row>
                )}
                <Grid.Row>
                    {this.renderVideosCards()}
                </Grid.Row>
            </LoaderComponent>
        )
    }

    render() {
        const SUBTABS = [{ title: `AI Transcribe (${formatCount(this.props.videosCounts.transcribe)})` }, { title: `Proofread (${formatCount(this.props.videosCounts.proofread)})` }, { title: `Completed (${formatCount(this.props.videosCounts.completed)})` }];
        return (
            <div>
                <VideosTabs
                    extraContent={(
                        <div
                            style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', }}
                        >
                            {SUBTABS.map((item, index) => (
                                <React.Fragment
                                    key={`subtab-item-${item.title}`}
                                >

                                    <span
                                        onClick={() => this.onTabChange(index)}
                                        style={{
                                            display: 'inline-block',
                                            cursor: 'pointer',
                                            marginRight: '2rem',
                                            textTransform: 'none',
                                            padding: '1rem',
                                            fontSize: '1rem',
                                            borderBottom: this.state.activeTab === index ? '3px solid #0e7ceb' : 'none',
                                            opacity: this.state.activeTab === index ? 1 : 0.5,
                                        }}
                                    >
                                        {item.title}
                                    </span>
                                    {index !== 2 && (
                                        <Icon name="chevron right" style={{ opacity: 0.5 }} />
                                    )}
                                </React.Fragment>
                            ))}
                            {/* <Tabs
                                items={[{ title: 'AI Transcribe' }, { title: 'Proofread' }, { title: 'Completed' }]}
                                onActiveIndexChange={(index) => this.onTabChange(index)}
                                activeIndex={this.state.activeTab}
                            /> */}
                        </div>
                    )}

                />

                <div>
                    <Grid style={{ marginTop: '1rem', }}>

                        <RoleRenderer roles={['admin', 'review']}>
                            <Grid.Row>
                                <Grid.Column width={5}>
                                    <Input
                                        fluid
                                        style={{ height: '100%' }}
                                        type="text"
                                        icon="search"
                                        iconPosition="left"
                                        input={<input
                                            type="text"
                                            style={{ borderRadius: 20, color: '#999999', backgroundColor: '#d4e0ed' }}
                                        />}

                                        placeholder="Search by file name, video name, etc"
                                        value={this.props.searchFilter}
                                        onChange={(e, { value }) => this.onSearchChange(value)}
                                    />
                                </Grid.Column>
                                <Grid.Column width={8}>
                                    <div className="pull-right" style={{ marginLeft: '2rem' }}>
                                        {this.renderPagination()}
                                    </div>
                                </Grid.Column>
                                <Grid.Column width={3}>
                                    <Dropdown
                                        className="pull-right"
                                        fluid
                                        search
                                        selection
                                        clearable
                                        placeholder="Language"
                                        onChange={this.onLanguageFilterChange}
                                        options={langsOptions}
                                        value={this.props.languageFilter}
                                    />
                                </Grid.Column>
                            </Grid.Row>
                            {this._renderTabContent()}
                            {this._renderDeleteVideoModal()}
                            {this.renderConfirmReviewModal()}
                            {this.renderEditVideoModal()}

                        </RoleRenderer>
                    </Grid>
                </div>
            </div>

        )
    }
}


const mapStateToProps = ({ organization, authentication, organizationVideos }) => ({
    organization: organization.organization,
    user: authentication.user,
    languageFilter: organizationVideos.languageFilter,
    videosLoading: organizationVideos.videosLoading,
    totalPagesCount: organizationVideos.totalPagesCount,
    currentPageNumber: organizationVideos.currentPageNumber,
    selectedVideo: organizationVideos.selectedVideo,
    searchFilter: organizationVideos.searchFilter,
    organizationUsers: organization.users,
    videos: organizationVideos.videos,
    videosCounts: organizationVideos.videosCounts,
})

const mapDispatchToProps = (dispatch) => ({
    fetchVideos: (params) => dispatch(videoActions.fetchVideos(params)),
    fetchVideosCount: (params) => dispatch(videoActions.fetchVideosCount(params)),
    updateVideo: (videoId, changes) => dispatch(videoActions.updateVideo(videoId, changes)),
    deleteVideo: videoId => dispatch(videoActions.deleteVideo(videoId)),
    setLanguageFilter: (langCode) => dispatch(videoActions.setLanguageFilter(langCode)),
    setCurrentPageNumber: pageNumber => dispatch(videoActions.setCurrentPageNumber(pageNumber)),
    setSelectedVideo: video => dispatch(videoActions.setSelectedVideo(video)),
    fetchUsers: (organizationId) => dispatch(organizationActions.fetchUsers(organizationId)),
    setVideoStatusFilter: filter => dispatch(videoActions.setVideoStatusFilter(filter)),
    setSearchFilter: filter => dispatch(videoActions.setSearchFilter(filter)),
    transcribeVideo: video => dispatch(videoActions.transcribeVideo(video)),
    skipTranscribe: video => dispatch(videoActions.skipTranscribe(video)),
    updateVideoReviewers: (videoId, users) => dispatch(videoActions.updateVideoReviewers(videoId, users)),
});



export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Review));