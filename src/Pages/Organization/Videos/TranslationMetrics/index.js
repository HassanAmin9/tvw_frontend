import React from 'react';
import { connect } from 'react-redux';

import { Link, withRouter } from 'react-router-dom';
import { Grid, Card, Button, Icon, Modal, Input, Popup } from 'semantic-ui-react';

import routes from '../../../../shared/routes';
import { isoLangs } from '../../../../shared/constants/langs';

import ArticleSummaryCard from '../../../../shared/components/ArticleSummaryCard';
import LoaderComponent from '../../../../shared/components/LoaderComponent';
import VideosTabs from '../VideosTabs';
import DeleteTranslationModal from './DeleteTranslationModal';
import AssignUsersSpeakersModal from '../../../../shared/components/AssignUsersSpeakersModal';

import * as videoActions from '../modules/actions';
import * as organizationActions from '../../../../actions/organization';
import { getUsersByRoles } from '../../../../shared/utils/helpers';
import ShowMore from '../../../../shared/components/ShowMore';
import AddHumanVoiceModal from '../../../../shared/components/AddHumanVoiceModal';
import DeleteBackgroundMusicModal from './DeleteBackgroundMusicModal';
import RoleRenderer from '../../../../shared/containers/RoleRenderer';
import websockets from '../../../../websockets';

class Translation extends React.Component {

    state = {
        selectedArticle: null,
        deleteArticleModalVisible: false,
        deleteBackgroundMusicModalVisible: false,
        assignUsersModalVisible: false,
    }

    componentWillMount = () => {
        const { videoId } = this.props.match.params;
        this.props.fetchUsers(this.props.organization._id);
        this.props.fetchSigleTranslatedArticle(videoId);
        this.initSocketSub();
    }

    componentWillUnmount = () => {
        websockets.unsubscribeFromEvent(websockets.websocketsEvents.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH)
    }

    initSocketSub = () => {
        this.socketSub = websockets.subscribeToEvent(websockets.websocketsEvents.EXTRACT_VIDEO_BACKGROUND_MUSIC_FINISH, (data) => {
            console.log('got socket data', data);
            const { videoId } = this.props.match.params;
            this.props.fetchSigleTranslatedArticle(videoId);
        })
    }

    getTranslators = () => {
        return getUsersByRoles(this.props.organizationUsers, this.props.organization, ['translate', 'admin', 'owner']);
    }

    isAdmin = () => {

    }

    deleteSelectedArticle = () => {
        const { videoId } = this.props.match.params;
        this.props.deleteArticle(this.state.selectedArticle._id, videoId);
        this.setState({ deleteArticle: null, deleteArticleModalVisible: false });
    }

    onUploadBackgroundMusic = (e, data) => {
        const file = e.target.files[0];
        const { singleTranslatedArticle } = this.props;
        this.props.uploadBackgroundMusic(singleTranslatedArticle.video._id, file);
    }

    onSaveTranslators = (translators) => {
        this.setState({ assignUsersModalVisible: false });
        this.props.updateTranslators(this.state.selectedArticle._id, translators.filter((t) => t.user));
    }

    onDeleteArticleClick = (article) => {
        this.setState({ selectedArticle: article, deleteArticleModalVisible: true });
    }

    onAddClick = (article) => {
        this.props.fetchUsers(this.props.organization._id);
        this.setState({ selectedArticle: article, assignUsersModalVisible: true });
    }

    onDeleteBackgroundMusic = () => {
        this.setState({ deleteBackgroundMusicModalVisible: false });
        this.props.deleteVideoBackgroundMusic(this.props.singleTranslatedArticle.video._id)
    }

    onDeleteBackgroundMusicClick = () => {
        this.setState({ deleteBackgroundMusicModalVisible: true });
    }

    getTranslationArticleUrl = (articleId, langCode, langName) => {
        const parts = [];
        if (langCode) {
            parts.push(`lang=${langCode}`);
        }
        if (langName) {
            parts.push(`langName=${langName}`);
        }

        const url = `${routes.translationArticle(articleId)}?${parts.join('&')}`
        return url;
    }

    onAddHumanVoice = (langCode, langName, translators) => {
        const { video } = this.props.singleTranslatedArticle;
        this.props.setAddHumanVoiceModalVisible(false);
        this.props.generateTranslatableArticle(video.article, langCode, langName, translators);
    }

    renderDeleteArticleModal = () => (
        <DeleteTranslationModal
            open={this.state.deleteArticleModalVisible}
            onClose={() => this.setState({ deleteArticleModalVisible: false, selectedArticle: null })}
            onConfirm={this.deleteSelectedArticle}
        />
    )

    renderDeleteBackgroundMusic = () => (
        <DeleteBackgroundMusicModal
            open={this.state.deleteBackgroundMusicModalVisible}
            onClose={() => this.setState({ deleteBackgroundMusicModalVisible: false })}
            onConfirm={this.onDeleteBackgroundMusic}
        />
    )

    _renderAddHumanVoiceModal() {
        const users = this.getTranslators();
        const { singleTranslatedArticle } = this.props;
        console.log(singleTranslatedArticle);
        console.log(singleTranslatedArticle && singleTranslatedArticle.originalArticle? singleTranslatedArticle.originalArticle.speakersProfile : '')
        return (
            <AddHumanVoiceModal
                open={this.props.addHumanVoiceModalVisible}
                onClose={() => this.props.setAddHumanVoiceModalVisible(false)}
                users={users}
                speakersProfile={singleTranslatedArticle && singleTranslatedArticle.originalArticle ? singleTranslatedArticle.originalArticle.speakersProfile : []}
                skippable={false}
                onSubmit={(langCode, langName, translators) => this.onAddHumanVoice(langCode, langName, translators)}
            />
        )
    }

    renderAssignUsersModal = () => {
        const users = this.getTranslators();

        return (
            <AssignUsersSpeakersModal
                open={this.state.assignUsersModalVisible}
                article={this.state.selectedArticle}
                users={users}
                onClose={() => this.setState({ assignUsersModalVisible: false })}
                onSave={this.onSaveTranslators}
            />
        )
    }

    render() {
        const { singleTranslatedArticle } = this.props;
        const users = this.getTranslators();

        return (
            <Grid
                style={{ marginTop: '1rem' }}
            >
                <Grid.Row>
                    <Grid.Column width={4}>
                        <Link to={routes.organziationTranslations()}>
                            <Button basic>
                                <Icon name={"arrow left"} /> Back
                        </Button>
                        </Link>
                    </Grid.Column>
                </Grid.Row>
                <LoaderComponent active={this.props.videosLoading && singleTranslatedArticle} >
                    {singleTranslatedArticle && (
                        <Grid.Row key={`translated-article-container-${singleTranslatedArticle.video._id}`} style={{ textAlign: 'center' }}>
                            <Grid.Column width={5}>
                                <Card fluid style={{ marginTop: -15 }}>
                                    <Card.Content>
                                        <Link to={routes.organizationArticle(singleTranslatedArticle.video.article)}>
                                            <h3>
                                                <ShowMore text={singleTranslatedArticle.video.title} length={20} />
                                            </h3>
                                        </Link>
                                    </Card.Content>
                                    <video src={singleTranslatedArticle.video.url} width="100%" height="100%" controls preload="false" />

                                    <Card.Content style={{ textAlign: 'left' }}>
                                        <Grid>
                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    <p>Translated in: <strong>{singleTranslatedArticle.articles ? singleTranslatedArticle.articles.length : 0} languages </strong></p>
                                                </Grid.Column>
                                            </Grid.Row>
                                            <Grid.Row>
                                                <Grid.Column width={16}>
                                                    <RoleRenderer roles={['admin']}>

                                                        <div
                                                            style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', fontSize: '0.7rem' }}
                                                        >
                                                            <div>
                                                                Background Music: <Popup
                                                                    content="Upload background music audio"
                                                                    trigger={(
                                                                        <Button
                                                                            basic
                                                                            size="tiny"
                                                                            icon="upload"
                                                                            style={{ fontSize: '0.7rem' }}
                                                                            onClick={() => this.backgroundMusicRef.click()}
                                                                        />
                                                                    )}
                                                                />
                                                            </div>
                                                            <span>
                                                                Or
                                                            </span>
                                                            <Popup
                                                                content="Automatically extract background music from the video"
                                                                trigger={(
                                                                    <Button
                                                                        primary
                                                                        loading={singleTranslatedArticle.video.extractBackgroundMusicLoading}
                                                                        disabled={singleTranslatedArticle.video.extractBackgroundMusicLoading}
                                                                        size="tiny"
                                                                        // fluid
                                                                        style={{ fontSize: '0.7rem' }}

                                                                        onClick={() => this.props.extractVideoBackgroundMusic(this.props.singleTranslatedArticle.video._id)}
                                                                    >
                                                                        Automatically Import
                                                                        </Button>
                                                                )}
                                                            />
                                                        </div>


                                                        <div
                                                            style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}
                                                        >
                                                            {singleTranslatedArticle.video.backgroundMusicUrl && (
                                                                <React.Fragment>

                                                                    <audio controls>
                                                                        <source src={singleTranslatedArticle.video.backgroundMusicUrl} />
                                                                    </audio>
                                                                    <Popup
                                                                        content="Delete background music file"
                                                                        trigger={(
                                                                            <Icon
                                                                                name="delete"
                                                                                color="red"
                                                                                style={{ marginLeft: 10, cursor: 'pointer' }}
                                                                                // size="large"
                                                                                onClick={this.onDeleteBackgroundMusicClick}
                                                                            />
                                                                        )}
                                                                    />
                                                                </React.Fragment>

                                                            )}
                                                        </div>
                                                        <input
                                                            accept="audio/*"
                                                            ref={(ref) => this.backgroundMusicRef = ref}
                                                            style={{ visibility: 'hidden' }}
                                                            type="file"
                                                            onChange={this.onUploadBackgroundMusic}
                                                        />
                                                    </RoleRenderer>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Card.Content>
                                    <Button fluid color="blue" onClick={() => {
                                        this.props.setAddHumanVoiceModalVisible(true);
                                    }}>Translate</Button>
                                    {/* <Button fluid color="blue" onClick={() => {
                                        this.props.setSelectedVideo(singleTranslatedArticle.video);
                                        this.props.setAddHumanVoiceModalVisible(true);
                                    }}>Translate</Button> */}
                                </Card>

                            </Grid.Column>
                            <Grid.Column width={11}>
                                <Grid>
                                    {singleTranslatedArticle.articles && singleTranslatedArticle.articles.length > 0 && (
                                        <Grid.Row style={{ padding: 20 }}>
                                            {singleTranslatedArticle.articles.map((article) => (
                                                <Grid.Column width={8} key={`translated-article-article-${article._id}`} style={{ marginBottom: 20 }}>
                                                    <ArticleSummaryCard
                                                        showOptions
                                                        showTranslators
                                                        users={users}
                                                        article={article}
                                                        lang={isoLangs[article.langCode] ? isoLangs[article.langCode].name : article.langCode}
                                                        onAddClick={() => this.onAddClick(article)}
                                                        onTitleClick={() => this.props.history.push(routes.translationArticle(article._id))}
                                                        onDeleteClick={() => this.onDeleteArticleClick(article)}
                                                    />
                                                </Grid.Column>
                                            ))}
                                        </Grid.Row>
                                    )}
                                </Grid>
                            </Grid.Column>
                        </Grid.Row>
                    )}
                    {this.renderDeleteArticleModal()}
                    {this.renderAssignUsersModal()}
                    {this._renderAddHumanVoiceModal()}
                    {this.renderDeleteBackgroundMusic()}
                </LoaderComponent>
            </Grid>
        )
    }
}



const mapStateToProps = ({ organizationVideos, organization }) => ({
    singleTranslatedArticle: organizationVideos.singleTranslatedArticle,
    videosLoading: organizationVideos.videosLoading,
    organization: organization.organization,
    addHumanVoiceModalVisible: organizationVideos.addHumanVoiceModalVisible,
    organizationUsers: organization.users,
})
const mapDispatchToProps = (dispatch) => ({
    setSelectedVideo: video => dispatch(videoActions.setSelectedVideo(video)),
    setAddHumanVoiceModalVisible: visible => dispatch(videoActions.setAddHumanVoiceModalVisible(visible)),
    setCurrentPageNumber: pageNumber => dispatch(videoActions.setCurrentPageNumber(pageNumber)),
    fetchSigleTranslatedArticle: (videoId) => dispatch(videoActions.fetchSigleTranslatedArticle(videoId)),
    uploadBackgroundMusic: (videoId, blob) => dispatch(videoActions.uploadBackgroundMusic(videoId, blob)),
    deleteArticle: (articleId, videoId) => dispatch(videoActions.deleteArticle(articleId, videoId)),
    setSearchFilter: filter => dispatch(videoActions.setSearchFilter(filter)),
    fetchUsers: (organizationId) => dispatch(organizationActions.fetchUsers(organizationId)),
    updateTranslators: (articleId, translators) => dispatch(videoActions.updateTranslators(articleId, translators)),
    generateTranslatableArticle: (originalArticleId, langCode, langName, translators) => dispatch(videoActions.generateTranslatableArticle(originalArticleId, langCode, langName, translators)),
    deleteVideoBackgroundMusic: (videoId) => dispatch(videoActions.deleteVideoBackgroundMusic(videoId)),
    extractVideoBackgroundMusic: (videoId) => dispatch(videoActions.extractVideoBackgroundMusic(videoId)),

})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Translation));
