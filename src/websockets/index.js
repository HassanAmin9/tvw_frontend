import io from 'socket.io-client';
import * as websocketsEvents from './events';

var pendingSubs = [];


var connection;
const createWebsocketConnection = function createWebsocketConnection(url, options = {}) {
  connection = io.connect(url, options);
  console.log('pending subs are', pendingSubs)
  if (pendingSubs.length > 0) {
    pendingSubs.forEach((sub) => {
      connection.on(sub.event, sub.callback);
    })
    pendingSubs = [];
  }
  return connection
}

const disconnectConnection = function disconnectConnection() {
  if (connection) return connection.disconnect();
}

const emitEvent = function emitEvent(event, args) {
  return connection.emit(event, args);
}

const subscribeToEvent = function subscribeToEvent(event, callback) {
  if (connection) {
    return connection.on(event, callback);
  } else {
    pendingSubs.push({ event, callback });
  }
}

const unsubscribeFromEvent = function unsubscribeFromEvent(event) {
  return connection.removeEventListener(event);
}

export default {
  createWebsocketConnection,
  disconnectConnection,
  emitEvent,
  subscribeToEvent,
  unsubscribeFromEvent,
  websocketsEvents,
}
