export default {
    home: () => '/',
    api: () => '/api',
    loginRedirect: () => '/lr',
    resetPassword: () => '/rp',
    logout: () => '/logout',
    demo: () => '/demo',
    convertProgress: (videoId = ':videoId') => `/convert/${videoId}`,
    convertProgressV2: (videoId = ':videoId') => `/convert/v2/${videoId}`,
    
    // Organization
    organizationHome: () => '/organization',
    organizationUsers: () => '/organization/users',
    organizationTips: () => '/organization/tips',
    organizationVideos: () => '/organization/videos',
    organizationArticle: (articleId = ':articleId') => `/organization/article/${articleId}`,
    organizationArchive: () => `/organization/archive`,

    organziationReview: () => `/organization/videos/review`,
    organziationTranslations: () => `/organization/videos/translations`,
    
    // organizationTasks: () => '/organization/tasks',
    organziationTasksReview: () => `/organization/tasks/review`,
    organziationTasksTranslations: () => `/organization/tasks/translations`,
    
    organziationTranslationMetrics: (videoId = ':videoId') => `/organization/videos/translations/${videoId}`,
    // Translation
    translationArticle: (articleId = ':articleId', langCode) => `/translation/article/${articleId}${langCode ? `?lang=${langCode}` : ''}`,

    translationInvitation: () => `/invitations/translate`,
    invitationsRoute: (organizationId = ':organizationId') => `/invitations/${organizationId}`,
}