import React from 'react';
import { Modal, Dropdown, Button } from 'semantic-ui-react';

function getUserName(user) {
    return user.firstname && user.lastname ? `${user.firstname} ${user.lastname} (${user.email})` : user.email;
}

export default class AssignReviewUsers extends React.Component {

    state = {
        value: [],
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.value !== this.state.value) {
            this.setState({ value: nextProps.value });
        }
    }

    onChange = (_, { value }) => {
        this.setState({ value });
    }

    render() {
        const options = this.props.users ? this.props.users.map((user) => ({ text: getUserName(user), value: user._id, key: `user-list-${user._id}` })) : [];

        return (
            <Modal open={this.props.open} size="tiny" onClose={this.props.onClose}>
                <Modal.Header>
                    Assign Users
                </Modal.Header>
                <Modal.Content>
                    <Dropdown
                        clearable
                        fluid
                        multiple
                        search
                        selection
                        options={options}
                        value={this.state.value}
                        onChange={this.onChange}
                        placeholder='Select users'
                    />
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.props.onClose}>
                        Cancel
                    </Button>
                    <Button
                        primary
                        onClick={() => this.props.onSave(this.state.value)}
                    >
                        Save
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}