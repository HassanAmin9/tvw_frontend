import React from 'react';
import { Card, Button, Icon, Dropdown } from 'semantic-ui-react';
import './style.scss';
import RoleRenderer from '../../containers/RoleRenderer';
import ShowMore from '../ShowMore';
import ReactPlayer from 'react-player';

class VideoCard extends React.Component {
    render() {
        const { showOptions, title, url, thumbnailUrl, buttonTitle, loading, disabled, onButtonClick, onDeleteVideoClick } = this.props;
        return (
            <div className="video-card">
                {showOptions && (
                    <RoleRenderer roles={['admin']}>
                        <Dropdown
                            direction="left"
                            icon={<Icon size="large" name="setting" color="blue" />}
                            className="video-card__dropdown-options"
                        >
                            <Dropdown.Menu>
                                <Dropdown.Item
                                    onClick={this.props.onEditClick}
                                >
                                    <Icon
                                        color="blue"
                                        name="edit"
                                        size="small"
                                        onClick={this.props.onEditClick}
                                    /> Edit
                                </Dropdown.Item>
                                <Dropdown.Item
                                    onClick={this.props.onAddClick}
                                >
                                    <Icon
                                        color="green"
                                        name="add"
                                        size="small"
                                        onClick={this.props.onAddClick}
                                    /> Add Reviewers
                                </Dropdown.Item>
                                <Dropdown.Item
                                    onClick={onDeleteVideoClick}
                                >
                                    <Icon
                                        name="trash alternate outline"
                                        color="red"
                                        size="small"
                                    /> Delete
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </RoleRenderer>

                )}
                <Card fluid>
                    <Card.Content>
                        <Card.Header className="video-card__header">
                            <ShowMore length={25} text={title} />
                        </Card.Header>
                    </Card.Content>
                    <ReactPlayer
                        url={url}
                        controls
                        width="100%"
                        height="150px"
                        style={{ minHeight: 150 }}
                        light={thumbnailUrl ? thumbnailUrl : false}
                        playing={thumbnailUrl ? true : false}
                    />
                    {/* <video src={url} controls preload={'false'} width={'100%'} /> */}
                    {this.props.extra ? this.props.extra : ''}
                    <Card.Content style={{ padding: 0 }}>
                        <div style={{ color: 'white' }}>
                            <Button
                                onClick={onButtonClick}
                                fluid
                                color="blue"
                                disabled={loading}
                                loading={disabled}
                            >
                                {buttonTitle}
                            </Button>
                        </div>
                    </Card.Content>
                </Card>
            </div>
        );
    }
}

export default VideoCard;